#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define n 50

struct data{
	char name[n];
	char lastname[n];
	char age[n];
	char email[n];
};

struct data line[] = {};

/* Función para ordenar datos */
void ordenar(char opt[], int reg, struct data line[])
{
	int i;
	int j;
	char aux[n];
	char ordenarvector[reg][n];
	for(i=0; i<=reg; i++)
	{
		for(j=0; j<i; j++)
		{
			if(opt[0] == 'a') // Ordenar registros por nombre
			{
				strcpy(ordenarvector[j], line[j].name);
				strcpy(ordenarvector[i], line[i].name);
			}
			else if(opt[0] == 'b') // Ordenar registros por apellido
			{
				strcpy(ordenarvector[j], line[j].lastname);
				strcpy(ordenarvector[i], line[i].lastname);
			}
			else if(opt[0] == 'c') // Ordenar registros por edad
			{
				strcpy(ordenarvector[j], line[j].age);
				strcpy(ordenarvector[i], line[i].age);
			}
			if(strcmp(ordenarvector[j], ordenarvector[i]) > 0) // Ordenar
			{
				strcpy(aux, line[j].name);
				strcpy(line[j].name, line[i].name);
				strcpy(line[i].name, aux);

				strcpy(aux, line[j].lastname);
				strcpy(line[j].lastname, line[i].lastname);
				strcpy(line[i].lastname, aux);

				strcpy(aux, line[j].age);
				strcpy(line[j].age, line[i].age);
				strcpy(line[i].age, aux);

				strcpy(aux, line[j].email);
				strcpy(line[j].email, line[i].email);
				strcpy(line[i].email, aux);
			}
		}
	}
}

/* Algoritmo Busqueda Binaria */
void search(char opt[], char buscar[], int reg, struct data line[])
{
	int i;
	int x = 0;
	int y = reg;
	int m = 0;
	char vectorabuscar[reg][n];
	char vectorname[reg][n];
	char vectorlastname[reg][n];
	char vectorage[reg][n];
	char vectoremail[reg][n];
	
	for(i=0; i<=reg; i++)
	{
		if(opt[0] == 'a') // Se buscará un nombre
		{
			strcpy(vectorabuscar[i], line[i].name);
		}
		else // Se buscará un apellido
		{
			strcpy(vectorabuscar[i], line[i].lastname);
		}
		
		strcpy(vectorname[i], line[i].name);
		strcpy(vectorlastname[i], line[i].lastname);
		strcpy(vectorage[i], line[i].age);
		strcpy(vectoremail[i], line[i].email);
	}
	
	while(1)
	{
		m = (x + y)/2;
		
		if(strcmp(buscar, vectorabuscar[m]) == 0)
		{
			printf("Registro\n%s, %s, %s, %s, encontrado en la posición %d\n", vectorname[m], vectorlastname[m], vectorage[m], vectoremail[m], m);
			break;
		}
		else if(strcmp(buscar, vectorabuscar[m]) < 0)
		{
			x = x;
			y = m - 1;
		}
		else
		{
			x = m + 1;
			y = y;
		}
	}
}

void check(char buscar[], int reg, struct data line[])
{
	int i;
	char opt[0];
	char aux[n];
	
	sprintf(aux, "%c%s%c", '"', buscar, '"');
	
	for(i=0; i<=reg; i++)
	{
		if(strcmp(aux, line[i].name) == 0) // Busca si es un nombre
		{
			printf("\nBuscando nombre...\n");
			opt[0] = 'a';
			ordenar(opt, reg, line);
			search(opt, aux, reg, line);
			break;
		}
		else if(strcmp(aux, line[i].lastname) == 0) // Busca si es un apellido
		{
			printf("\nBuscando apellido...\n");
			opt[0] = 'b';
			ordenar(opt, reg, line);
			search(opt, aux, reg, line);
			break;
		}
		else if(i == reg)
		{
			printf("\nNombre o apellido no encontrado\n");
			break;
		}
	}
}

int main(void)
{
	char filename[] = {};
	char buscar[n];
	char opt[0];
	int i = 0;
	int reg = 0;

	FILE *file;
	FILE *savefile;

	printf("\nIntroduzca el nombre del archivo para cargar los datos: ");
	scanf("%s", filename);

	file = fopen(filename, "rt");
	savefile = fopen("datos_organizados.txt", "wt");

	/* Cargar archivo */
	if(file == NULL)
	{
		printf("\nArchivo no encontrado\n\n");
		exit(1);
	}
	else
	{
		/* Cargar datos */
		for(i=0; fscanf(file, "%[^,], %[^,], %[^,], %s\n", line[i].name, line[i].lastname, line[i].age, line[i].email) != EOF; i++)
		{
			reg = i;
		}
		fclose(file);
		printf("\nDatos cargados satisfactoriamente\n");
	}

	/* Menu Principal */
	printf("\nAplicación Prueba Práctica Desarrollador.\n\
 a. Listar datos ordenados por apellido.\n\
 b. Listar datos ordenados por edad.\n\
 c. Guardar datos ordenados en archivo.\n\
 d. Realizar búsqueda binaria.\n\
	\nSeleccione una opción: ");
	
	scanf("%s", opt);
	
	switch(opt[0])
	{
		/* Listar datos en pantalla ordenados por apellido */
		case 'a': opt[0] = 'b';
		ordenar(opt, reg, line);
		printf("\nReg, Name, Last Name, Age, E-mail\n");
		for(i=0; i<=reg; i++)
		{
			printf("%d, %s, %s, %s, %s\n", i, line[i].name, line[i].lastname, line[i].age, line[i].email);
		}
		printf("\nProceso Completado\n");
		break;
		/* Listar datos en pantalla ordenados por edad */
		case 'b': opt[0] = 'c';
		ordenar(opt, reg, line);
		printf("\nReg, Name, Last Name, Age, E-mail\n");
		for(i=0; i<=reg; i++)
		{
			printf("%d, %s, %s, %s, %s\n", i, line[i].name, line[i].lastname, line[i].age, line[i].email);
		}
		printf("\nProceso Completado\n");
		break;
		/* Guardar datos organizados por nombre en archivo datos_organizados.txt */
		case 'c': opt[0] = 'a';
		ordenar(opt, reg, line);
		for(i=0; i<=reg; i++)
		{
			fprintf(savefile, "%s,%s,%s,%s\n", line[i].name, line[i].lastname, line[i].age, line[i].email);
		}
		fclose(savefile);
		printf("\nPor defecto, los datos son organizados por nombre y almacenados en archivo datos_organizados.txt\n");
		break;
		/* Busqueda binaria */
		case 'd': printf("\nIngrese el nombre o apellido que desea buscar: ");
		scanf("%s", buscar);
		check(buscar, reg, line);
		break;
		default: printf("\nOpción no Válida\n");
	}

	return 0;
}
